<?php


class HourlyProject extends Project
{
	protected $givenHours;
	protected $hoursSpent;
	protected $costHour;


	public function __construct($title, $description, $givenHours, $hoursSpent, $costHour)
	{
		parent::__construct($title, $description);

		$this->givenHours = $givenHours;
		$this->hoursSpent = $hoursSpent;
		$this->costHour = $costHour;
	}


	public function getPrice()
	{
		return $this->givenHours * $this->costHour;
	}

	public function getProjectProgress()
	{
		return $this->hoursSpent * 100 / $this->givenHours.' % ';
	}
}