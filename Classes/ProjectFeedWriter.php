<?php


class ProjectFeedWriter
{
	protected $projects = [];

	public function addProject(Workable $project){
		$this->projects[] = $project;
	}


	 public function hourlyProjectWrite(){
		$hourlyProject = "";
		 $hourlyProject.= '<div>';
		 $hourlyProject.= '<table width="30%" cellspacing="0" cellpadding="4" border="1" >';
		 $hourlyProject.= '<thead>';
		 $hourlyProject.= '<tr>';
		 $hourlyProject.= '<th scope="col">title</th>';
		 $hourlyProject.= '<th scope="col">progress, %</th>';
		 $hourlyProject.= '<th scope="col">price, $</th>';
		 $hourlyProject.= '<tr>';
		 $hourlyProject.= '</thead>';
		 $hourlyProject.= '<tbody>';

		 foreach ($this->projects as $project){
			 $hourlyProject.= '<tr>';
			 $hourlyProject.= '<td width="100">'. $project->getTitle().'</td>';
			 $hourlyProject.= '<td width="100">'.$project->getProjectProgress().'</td>';
			 $hourlyProject.= '<td width="100">'. $project->getPrice().' $'.'</td>';
			 $hourlyProject.= '</tr>';
		 }

		 $hourlyProject.= '</tbody>';
		 $hourlyProject.= '</table>';
		 $hourlyProject.= '</div>';

		 return $hourlyProject;
	 }

	 public function milestoneProjectWrite(){
		 $milestoneProject = "";
		 $milestoneProject.= '<div>';

		 foreach ($this->projects as $project){
			 $milestoneProject.= '<h3>'.$project->getTitle().'</h3>';
			 $milestoneProject.= '<ul>';
			 $milestoneProject.= '<li>'.' Completed: '.$project->getProjectProgress().'</li>';
			 $milestoneProject.= '<li>'.'Total price: '.$project->getPrice().' $'.'</li>';
			 $milestoneProject.= '</ul>';
		 }

		 $milestoneProject.= '</div>';

		 return $milestoneProject;
	 }

	 public function TaskWrite(){
		 $task = "";
		 $task.= '<div>';
		 foreach ($this->projects as $project){
			 $task.= '<h3>'.$project->getTitle().'</h3>';
			 $task.= '<ul>';
			 $task.= '<li>'.'Total price: '.$project->getPrice().' $'.'</li>';
			 $task.= '</ul>';
		 }
		 $task.= '</div>';

		 return $task;
	 }



}