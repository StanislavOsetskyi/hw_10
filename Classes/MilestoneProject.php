<?php


class MilestoneProject extends Project
{
	protected $allStages;
	protected $currentStage;
	protected $projectPrice;

	public function __construct($title, $description, $allStages, $currentStage, $projectPrice)
	{
		parent::__construct($title, $description);

		$this->allStages = $allStages;
		$this->currentStage = $currentStage;
		$this->projectPrice = $projectPrice;
	}

	public function getPrice()
	{
		return $this->projectPrice;
	}

	public function getProjectProgress()
	{
		return $this->currentStage * 100 / $this->allStages. '%';
	}
}