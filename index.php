<?php
require_once 'interfaces/Workable.php';
require_once 'Classes/Project.php';
require_once 'Classes/HourlyProject.php';
require_once 'Classes/MilestoneProject.php';
require_once 'Classes/Task.php';
require_once 'Classes/ProjectFeedWriter.php';



$hourlyProjectObj = [];
$hourlyProjectObj[] = new HourlyProject('Translate', 'translate text', '24', '6','10');
$hourlyProjectObj[] = new HourlyProject('Fix', 'error correction', '8', '4','40');

$milestoneProjectObj = [];
$milestoneProjectObj[] = new MilestoneProject('Vocabulary', 'english & russian', '5', '3','300');
$milestoneProjectObj[] = new MilestoneProject('Shop', 'bicycles', '10', '8','1500');

$taskObj = [];
$taskObj[] = new Task('Database','create database','100');
$taskObj[] = new Task('Diary','create diary','30');

echo "<h1> Hourly Projects </h1>";

$hourlyProjectWriter = new ProjectFeedWriter();

foreach ($hourlyProjectObj as $hourlyProject){
	$hourlyProjectWriter->addProject($hourlyProject);
}
echo $hourlyProjectWriter->hourlyProjectWrite();

echo "<h1> Milestone Projects </h1>";

$milestoneProjectWriter = new ProjectFeedWriter();

foreach ($milestoneProjectObj as $milestoneProject){
	$milestoneProjectWriter->addProject($milestoneProject);
}
echo $milestoneProjectWriter->milestoneProjectWrite();

echo "<h1> Tasks </h1>";

$taskWriter = new ProjectFeedWriter();

foreach ($taskObj as $task){
	$taskWriter->addProject($task);
}
echo $taskWriter->TaskWrite();


